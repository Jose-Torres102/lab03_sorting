package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;

import interfaces.Sorter;
import sorterClasses.AbstractSorter;
import sorterClasses.BubbleSortSorter;

public class EnteroTester {


    
    public static void main(String[] args) {
        test("Sorting Using DefaultComp", null);
        

    }
    
    
    public static void test(String msg, Comparator<Entero> cmp) {
        Entero[] arr = new Entero[5];
        arr[0] = new Entero(6);
        arr[1] = new Entero(9);
        arr[2] = new Entero(1);
        arr[3] = new Entero(3);
        arr[4] = new Entero(8);

        
        System.out.println("\n\n*******************************************************");
        System.out.println("*** " + msg + "  ***");
        System.out.println("*******************************************************");
        
//        for (int s=0; s<arr.length; s++) {
            Sorter<Entero> sorter = new BubbleSortSorter<Entero>(); 
            sorter.sort(arr, cmp);
            showArray(sorter.getName() + ": ", arr); 
        
//    }
}
    

    private static void showArray(String msg, Entero[] a) {
        System.out.print(msg); 
        for (int i=0; i<a.length; i++) 
            System.out.print("\t" + a[i]); 
        System.out.println();
    }

}